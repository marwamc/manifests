# k8s-config

deploy a service mesh to a cluster - including dependencies.

## system requirements
To drive things in this repo, the following packages are required:

1. [GNU Make][]
2. [kubectl][]
3. [kustomize][]
4. [argocd][]
5. [helm][]

## applications installed
List of applications installed, by order of installation:

| order |       app name       | version |  namespace   |  Status  |
|:-----:|:--------------------:|:-------:|:------------:|:--------:|
|   0   |         k8tz         |   N/A   |     k8tz     | Deployed |
|   1   |     cert-manager     | v1.8.0  | cert-manager | Deployed |
|   2   |        istio         | v1.14.0 | istio-system | Deployed |
|   3   |    oauth2-proxy      | latest  |   default    | Deployed |


[GNU Make]: https://www.gnu.org/software/make/
[kubectl]: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
[kustomize]: https://kubectl.docs.kubernetes.io/installation/kustomize/
[argocd]: https://argo-cd.readthedocs.io/en/stable/cli_installation/
[helm]: https://github.com/helm/helm/releases
