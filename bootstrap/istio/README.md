## istio kustomization

Refer to istio-csr examples for [istio configured with istio-csr][].

[See this for another example][] that shows installing istio with pa rivate CA.

#### get istioctl
```shell
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.16.1 TARGET_ARCH=x86_64 sh -
```

### generate manifests
```shell
istioctl manifest generate -f 6-istio/install-profile.yaml -o 6-istio/manifests
```

**NOTE:** 
Need to replace all instances of `policy/v1beta1` with `policy/v1`

### build manifests
```shell
kustomize build 6-istio/manifests -o 6-istio/istio.yaml
```

### dry-run
```shell
kubectl apply -k 6-istio/istio.yaml --dry-run="client"
kubectl apply -f 6-istio/istio.yaml --dry-run="server"
```

### install
```shell
kubectl apply --wait=true -f 6-istio/istio.yaml
```


[istio configured with istio-csr]: https://github.com/cert-manager/istio-csr/blob/main/docs/istio-config-getting-started.yaml
[See this for another example]: https://smallstep.com/blog/istio-with-private-ca/
