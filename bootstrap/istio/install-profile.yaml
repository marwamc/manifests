apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
metadata:
  namespace: istio-system
spec:
  hub: docker.io/istio
  tag: 1.16.1

  meshConfig:
    accessLogFile: /dev/stdout
    enableAutoMtls: true
    enableTracing: true
    trustDomain: cluster.local
    defaultConfig:
      tracingServiceName: CANONICAL_NAME_AND_NAMESPACE
      holdApplicationUntilProxyStarts: true
      proxyMetadata: {}
      tracing:
        sampling: 1
        zipkin:
          address: zipkin.istio-system.svc.cluster.local:9411
    enablePrometheusMerge: true
    # Opt-out of global http2 upgrades.
    # Destination rule is used to opt-in.
    # h2_upgrade_policy: DO_NOT_UPGRADE
    extensionProviders:
      - name: oauth2-proxy
        envoyExtAuthzHttp:
          service: oauth2-proxy.auth.svc.cluster.local
          port: 4180
          includeRequestHeadersInCheck:
          - cookie
          headersToUpstreamOnAllow:
          - authorization
          headersToDownstreamOnDeny:
          - set-cookie
      - name: zipkin
        zipkin:
          service: zipkin.istio-system.svc.cluster.local
          port: 9411

  # Traffic management feature
  components:
    base:
      enabled: true

    pilot:
      enabled: true
      k8s:
        env:
          # Disable istiod CA Sever functionality
          - name: ENABLE_CA_SERVER
            value: "false"
        overlays:
          - apiVersion: apps/v1
            kind: Deployment
            name: istiod
            patches:

              # Mount istiod serving and webhook certificate from Secret mount
            - path: spec.template.spec.containers.[name:discovery].args[-1]
              value: "--tlsCertFile=/etc/cert-manager/tls/tls.crt"
            - path: spec.template.spec.containers.[name:discovery].args[-1]
              value: "--tlsKeyFile=/etc/cert-manager/tls/tls.key"
            - path: spec.template.spec.containers.[name:discovery].args[-1]
              value: "--caCertFile=/etc/cert-manager/ca/root-cert.pem"

            - path: spec.template.spec.containers.[name:discovery].volumeMounts[-1]
              value:
                name: cert-manager
                mountPath: "/etc/cert-manager/tls"
                readOnly: true
            - path: spec.template.spec.containers.[name:discovery].volumeMounts[-1]
              value:
                name: ca-root-cert
                mountPath: "/etc/cert-manager/ca"
                readOnly: true

            - path: spec.template.spec.volumes[-1]
              value:
                name: cert-manager
                secret:
                  secretName: istiod-tls
            - path: spec.template.spec.volumes[-1]
              value:
                name: ca-root-cert
                configMap:
                  defaultMode: 420
                  name: istio-ca-root-cert

    # Istio Gateway feature
    ingressGateways:
    - name: istio-ingressgateway
      enabled: true

    egressGateways:
    - name: istio-egressgateway
      enabled: false

    # Istio CNI feature
    cni:
      enabled: true

    # Remote and config cluster configuration for an external istiod
    istiodRemote:
      enabled: false

  # Global values passed through to helm global.yaml.
  # Please keep this in sync with manifests/charts/global.yaml
  values:
    defaultRevision: ""
    global:
      caAddress: cert-manager-istio-csr.cert-manager.svc:443
      istioNamespace: istio-system
      istiod:
        enableAnalysis: false
      logging:
        level: "default:info"
      logAsJson: false
      pilotCertProvider: istiod
      jwtPolicy: third-party-jwt
      proxy:
        image: proxyv2
        clusterDomain: "cluster.local"
        resources:
          requests:
            cpu: 100m
            memory: 128Mi
          limits:
            cpu: 500m
            memory: 1024Mi
        logLevel: debug
        componentLogLevel: "misc:error"
        privileged: false
        enableCoreDump: false
        statusPort: 15020
        readinessInitialDelaySeconds: 1
        readinessPeriodSeconds: 2
        readinessFailureThreshold: 5
        includeIPRanges: "*"
        excludeIPRanges: ""
        excludeOutboundPorts: ""
        excludeInboundPorts: ""
        autoInject: enabled
      proxy_init:
        image: proxyv2
        resources:
          limits:
            cpu: 2000m
            memory: 1024Mi
          requests:
            cpu: 10m
            memory: 10Mi
      # Specify image pull policy if default behavior isn't desired.
      # Default behavior: latest images will be Always else IfNotPresent.
      imagePullPolicy: ""
      operatorManageWebhooks: false
      tracer:
        lightstep: {}
        zipkin: {}
        datadog: {}
        stackdriver: {}
      imagePullSecrets: []
      oneNamespace: false
      defaultNodeSelector: {}
      configValidation: true
      multiCluster:
        enabled: false
        clusterName: ""
      omitSidecarInjectorConfigMap: false
      network: ""
      defaultResources:
        requests:
          cpu: 10m
      defaultPodDisruptionBudget:
        enabled: true
      priorityClassName: ""
      useMCP: false
      sds:
        token:
          aud: istio-ca
      sts:
        servicePort: 0
      meshNetworks: {}
      mountMtlsCerts: false
    base:
      enableCRDTemplates: false
      validationURL: ""
    pilot:
      autoscaleEnabled: false
      autoscaleMin: 1
      autoscaleMax: 2
      replicaCount: 1
      image: pilot
      traceSampling: 1.0
      env: {}
      cpu:
        targetAverageUtilization: 80
      nodeSelector: {}
      keepaliveMaxServerConnectionAge: 30m
      enableProtocolSniffingForOutbound: true
      enableProtocolSniffingForInbound: true
      deploymentLabels:
      podLabels: {}
      configMap: true

    telemetry:
      enabled: true
      v2:
        enabled: false
        metadataExchange:
          wasmEnabled: false
        prometheus:
          wasmEnabled: false
          enabled: true
        stackdriver:
          enabled: false
          logging: false
          monitoring: false
          topology: false
          configOverride: {}

    istiodRemote:
      injectionURL: ""

    gateways:
      istio-egressgateway:
        env: {}
        autoscaleEnabled: false
        type: ClusterIP
        name: istio-egressgateway
        secretVolumes:
          - name: egressgateway-certs
            secretName: istiod-tls
            mountPath: /etc/istio/egressgateway-certs
          - name: egressgateway-ca-certs
            secretName: certificate-domain
            mountPath: /etc/istio/egressgateway-ca-certs

      istio-ingressgateway:
        autoscaleEnabled: true
        type: LoadBalancer
        name: istio-ingressgateway
        env: {}
        secretVolumes:
          - name: ingressgateway-certs
            secretName: istiod-tls
            mountPath: /etc/istio/ingressgateway-certs
          - name: ingressgateway-ca-certs
            secretName: certificate-domain
            mountPath: /etc/istio/ingressgateway-ca-certs
