# cert-manager installation

Default install instructions are [here][cert-manager-install]

## get cert manager manifests
```shell
curl -sSL https://github.com/cert-manager/cert-manager/releases/download/v1.8.0/cert-manager.yaml -o cert-manager.yaml
```



## References:

1. [cert-manager-getting-started][]


[cert-manager-install]: https://cert-manager.io/docs/installation/#default-static-install
[cert-manager-getting-started]: https://cert-manager.io/docs/installation/#getting-started
