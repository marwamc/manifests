# initial certificates

istio needs an initial certificate, we create that here.
See instructions [here][].

## 1. create istio root ca

```shell

kustomize build 4-issuer -o 4-issuer/.bad.issuers.yaml

kubectl apply -f 4-issuer/.bad.issuers.yaml --dry-run="client"
kubectl apply -f 4-issuer/.bad.issuers.yaml --dry-run="server"

kubectl apply --wait=true -f 4-issuer/.bad.issuers.yaml
```

## 2. copy the root ca created above to the cert-manager namespace

```shell
rm -f tmp/istio-ca.pem 

# istio-csr needs the root cert - so get it
kubectl get -n istio-system secret istio-ca \
  -ogo-template='{{index .data "tls.crt"}}' \
  | base64 -d > tmp/istio-ca.pem

# create root ca as secret in cert-manager namespace where itio-csr lives
kubectl create secret generic \
  -n cert-manager istio-root-ca \
  --from-file=ca.pem=tmp/istio-ca.pem \
  --from-file=root-cert.pem=tmp/istio-ca.pem
```

### references
1. https://github.com/cert-manager/aws-privateca-issuer/tree/main/config/examples
2. https://istio.io/latest/docs/tasks/security/cert-management/custom-ca-k8s/#part-2-using-custom-ca

[here]: https://cert-manager.io/docs/tutorials/istio-csr/istio-csr/#export-the-root-ca-to-a-local-file
