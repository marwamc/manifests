# openebs-provisioner

We'll use openebs-nfs-provisioner to get PVCs that support ReadWriteMany.
The default gp2 or do-block-storage StorageClass types only support ReadWriteOnce.

## prep step

Everything else in here is kustomize so we'll convert the openebs-nfs-provisioner 
helm chart into raw k8s manifests, which kustomize will then handle.

Use kustomize to render the openebs-nfs-provisioner helm chart into the 
base directory.

### render prepared base manifests

```shell
# preview the rendered template
kubectl kustomize --enable-helm ./prepare

# render to base dir
kubectl kustomize --enable-helm ./prepare | tee ./base/openebs-nfs-provisioner.yaml
```

## References

1. https://github.com/openebs/dynamic-nfs-provisioner

2. https://openebs.github.io/dynamic-nfs-provisioner

2. https://github.com/digitalocean/marketplace-kubernetes/blob/master/stacks/openebs-nfs-provisioner/values.yml

3. https://cloud.google.com/anthos-config-management/docs/how-to/use-repo-kustomize-helm#helm_chart_examples
