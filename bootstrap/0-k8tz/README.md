# k8tz

Pods get their time from the pod kernel, which could be wildly off.

It's important pods have the right time - aws sts has a time-based check 
that will not pass if the pod times are off.

## See SO issues

1. https://stackoverflow.com/questions/48949090/what-time-is-it-in-a-kubernetes-pod

2. https://stackoverflow.com/questions/61862329/getting-error-using-credentials-to-get-accountidgetcalleridentity-in-aws-terra

## enter k8tz

Follow install instructions [here][]

```shell
helm repo add k8tz https://k8tz.github.io/k8tz/

helm repo update

helm install k8tz k8tz/k8tz \
    --set timezone=US/Eastern \
    --set injectionStrategy=hostPath
```

[here]: https://github.com/k8tz/k8tz#installation
